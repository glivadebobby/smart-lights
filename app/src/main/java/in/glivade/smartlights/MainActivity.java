package in.glivade.smartlights;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.ghyeok.stickyswitch.widget.StickySwitch;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_LOCATION_PERMISSION = 1;
    private static final int REQUEST_ENABLE_GPS = 2;

    private StickySwitch stickySwitch;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private MyPreference preference;
    private boolean requestingLocationUpdates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initObjects();
        initCallbacks();
        createLocationRequest();
        configureLocation();
        setIntervalText();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (requestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_GPS) {
            if (resultCode == RESULT_OK) {
                if (requestingLocationUpdates) startLocationUpdates();
            } else {
                Toast.makeText(this, "Please enable GPS", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initObjects() {
        stickySwitch = findViewById(R.id.location);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = new LocationRequest();
        preference = new MyPreference(this);
    }

    private void initCallbacks() {
        stickySwitch.setOnSelectedChangeListener(new StickySwitch.OnSelectedChangeListener() {
            @Override
            public void onSelectedChange(StickySwitch.Direction direction, String s) {
                if (StickySwitch.Direction.LEFT == direction) {
                    requestingLocationUpdates = false;
                    stopLocationUpdates();
                } else if (StickySwitch.Direction.RIGHT == direction) {
                    requestingLocationUpdates = true;
                    startLocationUpdates();
                }
            }
        });
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                for (Location location : locationResult.getLocations()) {
                    updateLocation(location.getLatitude(), location.getLongitude());
                }
            }
        };
    }

    private void createLocationRequest() {
        locationRequest.setInterval(preference.getLocationInterval() * 1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void configureLocation() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                if (requestingLocationUpdates) startLocationUpdates();
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(MainActivity.this, REQUEST_ENABLE_GPS);
                    } catch (IntentSender.SendIntentException sendEx) {
                        sendEx.printStackTrace();
                    }
                }
            }
        });
    }

    private void setIntervalText() {
        SpannableString spannableString = new SpannableString(String.format(Locale.getDefault(),
                getString(R.string.txt_settings_interval), preference.getLocationInterval()));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                showIntervalDialog();
            }
        };
        spannableString.setSpan(clickableSpan, spannableString.length() - 11,
                spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        TextView textView = findViewById(R.id.txt_interval);
        textView.setText(spannableString);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.TRANSPARENT);
    }

    @SuppressLint("InflateParams")
    private void showIntervalDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View intervalView = LayoutInflater.from(this).inflate(R.layout.dialog_interval, null);
        builder.setView(intervalView);

        final TextInputLayout layoutInterval = intervalView.findViewById(R.id.interval);
        final TextInputEditText editTextInterval = intervalView.findViewById(R.id.input_interval);
        Button buttonSubmit = intervalView.findViewById(R.id.btn_submit);

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        editTextInterval.setText(String.valueOf(preference.getLocationInterval()));
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String interval = editTextInterval.getText().toString().trim();
                int duration = 0;
                try {
                    duration = Integer.parseInt(interval);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                if (interval.isEmpty()) {
                    layoutInterval.setError("Cannot be empty");
                } else if (duration < 10) {
                    layoutInterval.setError("Interval must be more than 10s");
                } else {
                    preference.setLocationInterval(duration);
                    createLocationRequest();
                    configureLocation();
                    setIntervalText();
                    alertDialog.dismiss();
                }
            }
        });
    }

    private void updateLocation(final double lat, final double lng) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                "http://smart-lights.glivade.in/index.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Toast.makeText(MainActivity.this,
                                    jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("tag", "add-location");
                params.put("lat", String.valueOf(lat));
                params.put("lng", String.valueOf(lng));
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "add-location");
    }

    private void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            fusedLocationProviderClient.requestLocationUpdates(locationRequest,
                    locationCallback, null);
        else requestLocationPermission();
    }

    private void stopLocationUpdates() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_PERMISSION);
    }
}
