package in.glivade.smartlights;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

public class MyPreference {

    private static final String PREF_LOCATION = "location";
    private static final String LOCATION_INTERVAL = "interval";
    private SharedPreferences mPreferencesLocation;

    public MyPreference(Context context) {
        mPreferencesLocation = context.getSharedPreferences(PREF_LOCATION, Context.MODE_PRIVATE);
    }

    public int getLocationInterval() {
        return mPreferencesLocation.getInt(encode(LOCATION_INTERVAL), 10);
    }

    public void setLocationInterval(int interval) {
        mPreferencesLocation.edit().putInt(encode(LOCATION_INTERVAL), interval).apply();
    }

    private String encode(String data) {
        return Base64.encodeToString(data.getBytes(), Base64.NO_WRAP);
    }
}
